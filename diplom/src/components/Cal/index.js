export default function Cal(props) {
  if (props.value) {
    return (
      <div className="column">
        <div className="row">Суточное потребление энергии: {Math.floor(props.value)} ккал</div>
        <div className="sep"></div>
      </div>
    )
  }

  return null
}