export default function CalResult(props) {
  if (props.value) {
    return (
      <div className="column">
        <div className="row">Суточное потребление энергии: {Math.floor(props.value)} ккал</div>
        <button className="link" onClick={() => props.changePage('feed')}></button>
        <div className="sep"></div>
        <div className="row row--end">
          <button className="action" onClick={() => props.changePage('feed')}>Подобрать рацион</button>
        </div>
      </div>
    )
  }

  return null
}