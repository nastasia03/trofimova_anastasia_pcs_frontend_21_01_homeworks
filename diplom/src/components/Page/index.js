import { Component } from 'react'

import CalResult from '../CalResult'
import Condition from '../Condition'

const defaultState = {
  gender: '',
  condition: '',
  lifestyle: '',
  weight: '',
  age: '',
  cal: '',
  page: 'calc',
  feed: '',
  ration: ''
}
const feeds = [
  'Almo Nature',
  'BarkingHeads',
  'Bozita',
  'Purina One'
]

class Page extends Component {
  constructor() {
    super()
    this.state = defaultState

    this.genderHandlerChange = this.genderHandlerChange.bind(this)
    this.conditionChangeHandler = this.conditionChangeHandler.bind(this)
    this.lifestyleChangeHandler = this.lifestyleChangeHandler.bind(this)
    this.weightChangeHandler = this.weightChangeHandler.bind(this)
    this.ageChangeHandler = this.ageChangeHandler.bind(this)
    this.clearState = this.clearState.bind(this)
    this.calcByCoefficient = this.calcByCoefficient.bind(this)
    this.calcCal = this.calcCal.bind(this)
    this.changePage = this.changePage.bind(this)
    this.feedChangeHandler = this.feedChangeHandler.bind(this)
  }

  get disabledCalc() {
    return !(this.state.gender && this.state.lifestyle && this.state.weight && this.state.age)
  }

  formHandler(event) {
    event.preventDefault()
  }
  genderHandlerChange(event) {
    this.setState(() => ({
      gender: event.target.value, 
      condition: undefined
    }))
  }
  conditionChangeHandler(event) {
    this.setState({ condition: event.target.value })
  }
  lifestyleChangeHandler(event) {
    this.setState({ lifestyle: event.target.value })
  }
  weightChangeHandler(event) {
    this.setState({ weight: event.target.value })
  }
  ageChangeHandler(event) {
    this.setState({ age: event.target.value })
  }
  clearState() {
    this.setState(defaultState)
  }
  calcByCoefficient(coefficient) {
    this.setState({
      cal: coefficient * (30 * this.state.weight + 70),
      feed: '',
      ration: ''
    })
  }
  calcCal() {
    if (this.state.age > 7) {
      this.calcByCoefficient(1.1)
      return
    } else {
      switch (this.state.lifestyle) {
        case 'active': {
          this.calcByCoefficient(1.6)
          return
        }
        case 'passive': {
          this.calcByCoefficient(1)
          return
        }
  
        default: { break }
      }

      switch (this.state.condition) {
        case 'castrated':
        case 'sterilized': {
          this.calcByCoefficient(1.2)
          return
        }
        case 'pregnancy': {
          this.calcByCoefficient(1.8)
          return
        }
        case 'lactation': {
          this.calcByCoefficient(4)
          return
        }
        default: {
          this.calcByCoefficient(1.4)
          return
        }
      }
    }
  }
  changePage(page) {
    this.setState({ page })
  }
  calcRationByFeed(ration) {
    this.setState({ ration: (this.state.cal * 100) / ration})
  }
  feedChangeHandler(event) {
    const feed = event.target.value
    this.setState({ feed })

    switch (feed) {
      case 'Almo Nature': {
        this.calcRationByFeed(401)
        break
      }
      case 'BarkingHeads': {
        this.calcRationByFeed(410)
        break
      }
      case 'Bozita': {
        this.calcRationByFeed(406)
        break
      }
      case 'Purina One': {
        this.calcRationByFeed(403)
        break
      }

      default: {
        return
      }
    }
  }

  render() {
    switch (this.state.page) {
      case 'feed': {
        return (
          <div className="calc__container">
            <select name="feed" id="feed" className="select" value={this.state.feed} onChange={this.feedChangeHandler}>
              <option value="">--Выберите производителя--</option>
              {feeds.map(feed => <option value={feed} selected={this.state.feed === feed} key={feed}>{feed}</option>)}
            </select>
            <div className="sep"></div>
              <p className="hint">
                Суточное потребление энергии: {Math.floor(this.state.cal)} ккал.<br />
                Рацион корма, в день: {Math.floor(this.state.ration)} грамм.
              </p>
            <div className="sep"></div>
            <div className="calc__form-row calc__form-row--end calc__form-row--actions">
              <button className="action" onClick={() => this.changePage('calc')}>Назад</button>
            </div>
          </div>
        )
      }
      default: {
        return (
          <div className="calc__container">
            <form className="calc__form" onSubmit={this.formHandler}>
              <div className="calc__form-row">
                <div className="column">
                  <div className="calc__form-title calc__form-title--required">Пол</div>
                  <div className="row">
                    <input type="radio" id="male" name="gender" value="male" checked={this.state.gender === 'male'} onChange={this.genderHandlerChange} />
                    <label htmlFor="male">Кот</label>
                    <input type="radio" id="female" name="gender" value="female" checked={this.state.gender === 'female'} onChange={this.genderHandlerChange} />
                    <label htmlFor="female">Кошка</label>
                  </div>
                </div>
              </div>
              <Condition value={this.state.condition} gender={this.state.gender} conditionChangeHandler={this.conditionChangeHandler} />
              <div className="calc__form-row">
                <div className="column">
                  <div className="calc__form-title calc__form-title--required">Образ жизни</div>
                  <div className="row">
                    <input type="radio" id="active" name="lifestyle" value="active" checked={this.state.lifestyle === 'active'} onChange={this.lifestyleChangeHandler} />
                    <label htmlFor="active">Активный</label>
                    <input type="radio" id="passive" name="lifestyle" value="passive" checked={this.state.lifestyle === 'passive'} onChange={this.lifestyleChangeHandler} />
                    <label htmlFor="passive">Пассивный</label>
                  </div>
                </div>
              </div>
              <div className="calc__form-row">
                <div className="column">
                  <div className="calc__form-title calc__form-title--required">Прочие данные</div>
                  <div className="inputs">
                    <input type="number" placeholder="Вес (кг)" min="2" step="0.1" value={this.state.weight} onChange={this.weightChangeHandler} />
                    <input type="number" placeholder="Возраст (полных лет)" min="1" step="1" value={this.state.age} onChange={this.ageChangeHandler} />
                  </div>
                </div>
              </div>
              <div className="sep"></div>
              <p className="hint">Поля отмеченные звёздочкой обязательны к заполнению.</p>
              <div className="sep"></div>
              <CalResult value={this.state.cal} changePage={this.changePage}/>
              <div className="calc__form-row calc__form-row--end calc__form-row--actions">
                <button className="action" onClick={this.clearState}>Сбросить</button>
                <button className="action" onClick={this.calcCal} disabled={this.disabledCalc}>Рассчитать калории</button>
              </div>
            </form>
          </div>
        )
      }
    }
  }
}

export default Page