export default function Condition(props) {
  if (props.gender === 'male') {
    return (
      <div className="column">
        <div className="calc__form-title">Состояние</div>
        <div className="row">
          <input type="radio" id="castrated" name="condition" value="castrated" checked={props.value === 'castrated'} onChange={props.conditionChangeHandler} />
          <label htmlFor="castrated">Кастрированный</label>
        </div>
      </div>
    )
  } else {
    return (
      <div className="column">
        <div className="calc__form-title">Состояние</div>
        <div className="row">
          <div className="row">
            <input type="radio" id="sterilized" name="condition" value="sterilized" checked={props.value === 'sterilized'} onChange={props.conditionChangeHandler} />
            <label htmlFor="sterilized">Стерилизованная</label>
          </div>
          <div className="row">
            <input type="radio" id="pregnancy" name="condition" value="pregnancy" checked={props.value === 'pregnancy'} onChange={props.conditionChangeHandler} />
            <label htmlFor="pregnancy">Беременность</label>
          </div>
          <div className="row">
            <input type="radio" id="lactation" name="condition" value="lactation" checked={props.value === 'lactation'} onChange={props.conditionChangeHandler} />
            <label htmlFor="lactation">Лактация</label>
          </div>
        </div>
      </div>
    )
  }
}