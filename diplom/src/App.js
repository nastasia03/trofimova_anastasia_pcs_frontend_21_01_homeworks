import { Component } from 'react'

import Page from './components/Page'

import './App.css'

class App extends Component {
  render() {
    return (
      <div className="calc">
        <div className="calc__title">
          <div className="column">
            <h1>Калькулятор калорий</h1>
            <p>Для расчета количества калорий в день для кошек и котов.</p>
          </div>
          <div className="logo" style={{backgroundImage: `url('${process.env.PUBLIC_URL}/image/cat.png')`}}></div>
        </div>
        <Page />
      </div>
    )
  }
}

export default App
