function makeFibonacciFunction() {
  let previousNumberA = 0
  let previousNumberB = 1

  return () => {
    let currentNumber = previousNumberA + previousNumberB
    previousNumberA = previousNumberB
    previousNumberB = currentNumber
    console.log(previousNumberA)
  }
}

const fibonacci = makeFibonacciFunction();

fibonacci(); // Вывод в консоль: 1
fibonacci(); // Вывод в консоль: 1
fibonacci(); // Вывод в консоль: 2
fibonacci(); // Вывод в консоль: 3
fibonacci(); // Вывод в консоль: 5