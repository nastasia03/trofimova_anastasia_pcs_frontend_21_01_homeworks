const rowEmail = document.getElementById('form__row-email');
const rowPassword = document.getElementById('form__row-password');
const rowCheckbox = document.getElementById('form__row-checkbox');
const inputEmail = document.getElementById('input__email');
const inputPassword = document.getElementById('input__password');
const inputCheckbox = document.getElementById('input__checkbox');
const messageEmail = document.getElementById('message__email');
const messagePassword = document.getElementById('message__password');
const messageCheckbox = document.getElementById('message__checkbox');

const button = document.getElementById('button');

const textRequired = 'Поле обязательно для заполнения'

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const buttonCallback = function (event) {
  event.preventDefault()

  let emailValidation = false
  let passwordValidation = false
  let checkboxValidation = false

  if (!inputEmail.value) {
    rowEmail.classList.add('invalid')
    messageEmail.style.display = 'block'
    messageEmail.textContent = textRequired
    emailValidation = false
  } else if (!validateEmail(inputEmail.value)) {
    rowEmail.classList.add('invalid')
    messageEmail.style.display = 'block'
    messageEmail.textContent = 'Email невалидный'
    emailValidation = false
  } else {
    rowEmail.classList.remove('invalid')
    messageEmail.style.display = null
    messageEmail.textContent = null
    emailValidation = true
  }

  if (!inputPassword.value) {
    rowPassword.classList.add('invalid')
    messagePassword.style.display = 'block'
    messagePassword.textContent = textRequired
    passwordValidation = false
  } else if (inputPassword.value && inputPassword.value.length < 8) {
    rowPassword.classList.add('invalid')
    messagePassword.style.display = 'block'
    messagePassword.textContent = 'Пароль должен содержать как минимум 8 символов'
    passwordValidation = false
  } else {
    rowPassword.classList.remove('invalid')
    messagePassword.style.display = null
    messagePassword.textContent = null
    passwordValidation = true
  }

  if (!inputCheckbox.checked) {
    rowCheckbox.classList.add('invalid')
    messageCheckbox.style.display = 'block'
    messageCheckbox.textContent = textRequired
    checkboxValidation = false
  } else {
    rowCheckbox.classList.remove('invalid')
    messageCheckbox.style.display = null
    messageCheckbox.textContent = null
    checkboxValidation = true
  }

  if (emailValidation && passwordValidation && checkboxValidation) {
    console.log({
      email: inputEmail.value,
      password: inputPassword.value
    })
  }
}

button.addEventListener('click', buttonCallback);