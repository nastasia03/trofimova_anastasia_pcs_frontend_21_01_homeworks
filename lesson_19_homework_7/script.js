function checkNumber(value) {
  value = parseFloat(value)
  if (isNaN(value)) {
    alert(`Некорректный ввод чисел`)
    return false
  }
  return value
}

function checkOperator(operator) {
  const result = !!~['+', '-', '/', '*'].indexOf(operator)
  if (!result) {
    alert('Программа не поддерживает такую операцию')
  }

  return result
}

let result = false
let index = 0

function calc() {
  do {
    index = 0

    let firstNumber = prompt('Введите первое число')
    if (!firstNumber) {
      alert(`Первое число не указано`)
      break
    }
    firstNumber = checkNumber(firstNumber)
    if (firstNumber || firstNumber === 0) {
      index++
    } else {
      break
    }
  
    let operator = prompt('Введите знак операции')
    if (!operator) {
      alert('Не введен знак')
      break
    }
  
    if (checkOperator(operator)) {
      index++
    } else {
      break
    }
  
    let secondNumber = prompt('Введите второе число')
    if (!secondNumber) {
      alert(`Второе число не указано`)
      break
    }
    secondNumber = checkNumber(secondNumber)
    if (secondNumber || secondNumber === 0) {
      switch (operator) {
        case '+': {
          result = firstNumber + secondNumber
          break
        }
        case '-': {
          result = firstNumber - secondNumber
          break
        }
        case '/': {
          result = firstNumber / secondNumber
          break
        }
        case '*': {
          result = firstNumber * secondNumber
          break
        }
      }
      console.log(result)
      index++
    } else {
      break
    }
  } while (index < 3);
}

calc()