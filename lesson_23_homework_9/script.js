// https://reqres.in/api/users
fetch('https://reqres.in/api/users?per_page=12')
  .then((response) => {
    return response.json()
  })
  .then((body) => {
    console.log('-----------');
    console.log('Пункт №1:')
    console.log('-----------');
    const users = body.data
    console.log(users)

    console.log('-----------');
    console.log('Пункт №2:')
    console.log('-----------');
    for (const user of users) {
      console.log(user.last_name)
    }
    console.log('-----------');
    console.log('Пункт №3:')
    console.log('-----------');
    const userF = users.filter(user => {
      return user.last_name[0] === 'F'
    })
    console.log(userF)

    console.log('-----------');
    console.log('Пункт №4:')
    console.log('-----------');
    const userName = users.reduce(
      (result, user) => {
        result.push(user.first_name + ' ' + user.last_name);
        return result;
      },
      []);
    console.log('Наша база содержит данные следующих пользователей: ' + userName.join(', '))

    console.log('-----------');
    console.log('Пункт №5:')
    console.log('-----------');
    for (const key in users[0]) {
      console.log(key)
    }
  });